/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patiement_management.userpackage;

import java.util.ArrayList;
import utilities.IObserver;
import utilities.ISubject;

public abstract class userclass implements ISubject  {
    
    
    protected Integer id;
    protected String FirstName;
    protected String LastName;
    protected String Address;
    protected String DateOfBirth;
    protected String UserName;
    protected String Password;
    //protected AddDel AddDel;
    private ArrayList<IObserver> observers = null;

    public userclass() {
    }
    
    public userclass (Integer id, String FirstName, String LastName, String Address, String DateOfBirth, String UserName, String Password){
    
    //public useclass(Integer idNum, String Fname, String Lname, String uAddress, String DBirth, String uName, String uPassword)
        
    this.id = id;
    this.FirstName = FirstName;
    this.LastName = LastName;
    this.Address = Address;
    this.DateOfBirth = DateOfBirth;
    this.UserName = UserName;
    this.Password = Password; 
    }
    
    public Integer getid(){
        return id;
    }
    
    public void  setid(Integer id){
        this.id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }
    

    public abstract UserType getUserType();
    
    @Override
    public  Boolean registerObserver(IObserver o){
        Boolean blnAdded = false;
        //check observer
        if (o != null) {
            if(this.observers == null) {
                this.observers = new ArrayList<>();  
            }
            
            if (!this.observers.contains(o)) {
                blnAdded = this.observers.add(o);
                
            }
        }
        
        return blnAdded;
    }
    
    @Override
    public Boolean removeObserver(IObserver o) {
        Boolean blnRemoved = false;
        if (o != null) {
            if (this.observers !=null && this.observers.size() > 0) {
                blnRemoved= this.observers.remove(o);
                
            }
        }
            
        return blnRemoved;
    }
        
    @Override
    public void notifyObservers() {
        if (this.observers != null && this.observers.size() > 0) {
            for (IObserver currentObserver : this.observers) {
                currentObserver.update();
            }
        }
    }
}
