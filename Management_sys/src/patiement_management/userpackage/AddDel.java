/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package patiement_management.userpackage;

import java.util.ArrayList;
import utilities.IObserver;
import utilities.ISubject;

public class AddDel implements ISubject, IObserver
{
    
    private ArrayList<userclass> nuserclass = new ArrayList ();
    private ArrayList<IObserver> observers = null;
    
    public void CreateNewAppointment()
    {
        nuserclass = new ArrayList();
        userclass item;
    }
    
  public Boolean addAppointment(userclass objNewItem)
    {
        Boolean blnResult = false;
        if (null != objNewItem)
        {
            if (!this.nuserclass.contains(objNewItem))
            {
                blnResult = this.nuserclass.add(objNewItem);
                if (blnResult)
                {
                    objNewItem.registerObserver(this);
                    this.notifyObservers();
                }
            }
        }
        return blnResult;
    }
    
    
    
    
    
public boolean removeAddDell(userclass objOldItem)
 {
     Boolean blnResult = false;
     if (null != objOldItem)
     {
        if (null != nuserclass && this.nuserclass.size() > 0)
         {
             blnResult = this.nuserclass.remove(objOldItem);
             if (blnResult) {
                 objOldItem.removeObserver(this);
                 this.notifyObservers();
                 
             }
         }
     }
     return blnResult;
 }

    @Override
    public Boolean registerObserver(IObserver o) {
        Boolean blnAdded = false;
        
        if (o != null)
        {
            if (this.observers == null)
            {
                this.observers = new ArrayList<>();
            }
            
            if (!this.observers.contains(o))
            {
                blnAdded = this.observers.add(o);
            }
           
        }
        return blnAdded;
    }

    @Override
    public Boolean removeObserver(IObserver o) {
        Boolean blnRemoved = false;
         if (o !=null)
         {
             if (this.observers != null && this.observers.size() > 0)
             {
                 blnRemoved = this.observers.remove(o);
             }
         }
         return blnRemoved;
    }

    @Override
    public void notifyObservers() {
        if (this.observers != null && this.observers.size() > 0) 
        {
            for(IObserver currentObserver : this.observers)
            {
                currentObserver.update();
            }
        }
    }

    @Override
    public void update() {
        this.notifyObservers();
    }
}