/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patiement_management.userpackage;

/**
 *
 * @author Sammy
 */
public class Admin extends userclass {
    
    public Admin()
    {
        super();
    }
    
    public Admin ( Integer id, String FirstName, String LastName, String Address, String DateOfBirth, String UserName, String Password)
    {
        super (id, FirstName, LastName, Address, DateOfBirth, UserName, Password);
    }
    
    @Override
    public UserType getUserType(){
        return UserType.ADMIN;
    }
    
}
